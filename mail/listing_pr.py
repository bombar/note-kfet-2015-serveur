#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Destiné à envoyé un mail récapitulatif des consos de la
    journée et des crédits/retraits espèces
    aux gens intéressés """

import sys
import mail
import argparse
import time

sys.path.append("../config")
import config

sys.path.append("../serveur")
import BaseFonctions

#: idbde du compte du PR
IDBDE_PR = 5537

#: Si aucune date n'est spécifiée, la collecte commence aujourd'hui à cette heure.
START_HOUR = "07:30:00"

#: Si aucune date n'est spécifiée, la collecte termine aujourd'hui à cette heure.
END_HOUR = "19:30:00"

def get_data(args):
    """Récupère les données intéressantes dans la BDD"""
    params = {"idbdepr" : args.idbdepr, "debut" : args.debut, "fin" : args.fin}
    req_consos = """
        SELECT description, sum(quantite) AS quantite, ROUND(sum(montant*quantite*CAST(valide AS int))/100.0, 2) AS montant
        FROM transactions
        WHERE %s
          AND date >= %%(debut)s
          AND date <= %%(fin)s
          AND type = 'bouton'
          AND valide
        GROUP BY description
        ORDER BY quantite DESC
        ;"""
    where_destinataire = "destinataire = %(idbdepr)s"
    where_emetteur = "emetteur = %(idbdepr)s"
    req_credret = """
        SELECT c.pseudo, ROUND(sum(t.montant*t.quantite)/100.0, 2) AS montant
        FROM transactions t, comptes c
        WHERE t.type = '%s'
          AND c.idbde = t.emetteur
          AND date >= %%(debut)s
          AND date <= %%(fin)s
        GROUP BY c.pseudo
        ORDER BY c.pseudo
        ;"""
    con, cur = BaseFonctions.getcursor()
    cur.execute(req_consos % (where_destinataire,), params)
    consos_dest = cur.fetchall()
    cur.execute(req_consos % (where_emetteur,), params)
    consos_em = cur.fetchall()
    cur.execute(req_credret % ("crédit",), params)
    credits = cur.fetchall()
    cur.execute(req_credret % ("retrait",), params)
    retraits = cur.fetchall()
    return consos_dest, consos_em, credits, retraits

def format(args, consos_dest, consos_em, credits, retraits):
    """Formate joliment tout le bordel."""
    ls = [consos_dest, consos_em, credits, retraits]
    titles = [
        u"Consos reçues par le compte %s" % args.idbdepr,
        u"Consos émises par le compte %s (devrait être vide)" % args.idbdepr,
        u"Somme des crédits",
        u"Somme des retraits"
    ]
    keys3 = ["description", "quantite", "montant"]
    keys2 = ["pseudo", "montant"]
    text = u"Résumé des activités note pendant le PR entre %s et %s.\n\n" % (args.debut, args.fin)
    for (l, title, keys) in zip(ls, titles, [keys3, keys3, keys2, keys2]):
        text += u"== %s ==\n" % (title,)
        if l:
            total = sum([i["montant"]for i in l])
            text += BaseFonctions.sql_pretty_print(l, keys=keys)
            text += "Total : %s\n" % (total,)
        else:
            text += u"(Rien)\n"
        text += u"\n"
    text += u"\n-- \nScript listing_pr.py"
    return text

parser = argparse.ArgumentParser("Listing (et envoi par mail) des consos PR + crédits/retraits")

parser.add_argument("-m", "--mail", action="store_true", help="Envoyer effectivement le mail.")
parser.add_argument("-i", "--idbdepr", action="store", help="Identifiant du compte PR", default=IDBDE_PR)
parser.add_argument("-d", "--debut", action="store", help="Timestamp de début (par défaut, 7h30 aujourd'hui)")
parser.add_argument("-f", "--fin", action="store", help="Timestamp de fin (par défaut, 18h30 aujourd'hui)")
parser.add_argument("-q", "--quiet", action="store_true", help="Ne pas parler sur stdout")
parser.add_argument("-c", "--custom-message", dest="message", action="store", help="Message supplémentaire à ajouter (dans la signature)")

if __name__ == "__main__":
    args = parser.parse_args()
    if args.debut is None and args.fin is None:
        today = time.strftime("%F")
        args.debut = "%s %s" % (today, START_HOUR)
        args.fin = "%s %s" % (today, END_HOUR)
    elif args.debut is None or args.fin is None:
        print u"Si tu fournis une date de début/fin, il faut aussi fournir une date de fin/début.".encode("utf-8")
        sys.exit(1)
    things = get_data(args)
    text = format(args, *things)
    if args.message:
        text += u"\n\n" + args.message.decode("utf-8")
    if not args.quiet:
        print text.encode("utf-8")
    if args.mail:
        mail.queue_mail(config.mails_from,
            destinataires=["tresorerie.bde@lists.crans.org"],
            cc=["paulon@crans.org","pollion@crans.org"],
            objet=u"Bilan du jour du PR",
            message=text)
        if not args.quiet:
            print u"\n=> Mail envoyé !".encode("utf-8")
    else:
        if not args.quiet:
            print u"\n=> (mail non envoyé)".encode("utf-8")
