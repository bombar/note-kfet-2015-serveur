#!/usr/bin/python
#-*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys
from datetime import datetime, timedelta

from jinja2 import Environment, PackageLoader
import psycopg2, psycopg2.extras

if '/home/note/note-kfet-2015-serveur/mail/' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/mail/')
from mail import queue_mail

sys.path.append('../serveur')
import BaseFonctions

# Initialise la connexion à la base de données
con, cur = BaseFonctions.getcursor()

# Récupère les informations du wei
cur.execute("SELECT prix_wei_normalien, prix_wei_non_normalien, wei_name, wei_contact, wei_begin, wei_end FROM configurations;")
wei_config = cur.fetchall()[0]

def human_roles(roles_list):
    """
        Transforme un rôle écrit au format BDD en rôle
        lisible par un être humain
    """
    liste = roles_list.split(';')
    liste_human = []

    for role in liste:
        if role == 'inconnu':
            liste_human.append('Rôle inconnu')
        elif role == 'libre':
            liste_human.append('Électron libre')
        elif role == 'chef_bus':
            liste_human.append('Chef de bus')
        elif role == 'chef_equipe':
            liste_human.append('Chef d\'équipe')
        elif role == 'staff':
            liste_human.append('Staff')

    return ' ou '.join(liste_human)

emetteur = 'notekfet2015@crans.org'
objet = '[WEI] État de ta préinscription'
reply_to = [wei_config["wei_contact"].decode('utf-8')]

if __name__ == '__main__':
    #  Chargement du template de mail
    env = Environment(loader=PackageLoader('mail', 'templates'))
    env.filters['human_roles'] = human_roles

    template = env.get_template('template_wei')

    # Récupération des inscriptions dans la base
    cur.execute("SELECT payé as paye,* FROM wei_vieux;")

    liste_inscriptions = cur.fetchall()

    # On traite les inscriptions
    for inscription in liste_inscriptions:

        paye = inscription['paye']
        caution = inscription['caution']
        preciser_bus = 'Je ne sais pas' == inscription['bus']
        preciser_role = ';' in inscription['role']

        if paye and caution and not (preciser_bus or preciser_role):
            # Rien à redire -> on passe à la suite
            continue

        else:
            cur.execute("SELECT * FROM comptes WHERE idbde=%s;", (inscription['idbde'],))
            user = cur.fetchone()
            # Si on est payé on paye le plein tarif.
            if inscription["normalien"] and not inscription["conge"]:
                prix_wei = wei_config["prix_wei_normalien"]
            else:
                prix_wei = wei_config["prix_wei_non_normalien"]
            peut_payer = user["solde"] >= prix_wei

            contexte = {
                'inscription' : inscription,
                'user' : user,
                'paye' : paye,
                'peut_payer' : peut_payer,
                'caution' : caution,
                'preciser_bus' : preciser_bus,
                'preciser_role' : preciser_role,
                'wei' : wei_config,
            }

            body = template.render(**contexte)

            queue_mail(emetteur, [inscription["mail"],], objet, body, cc=[], replyto=reply_to)
