#!/usr/bin/python
#-*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys
from datetime import datetime, timedelta

from jinja2 import Environment, PackageLoader
import psycopg2, psycopg2.extras

if '/home/note/note-kfet-2015-serveur/mail/' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/mail/')
from mail import queue_mail

sys.path.append('../serveur')
import BaseFonctions

emetteur = 'La Tresorerie du BdE <tresorerie.bde@lists.crans.org>'
objet = '[Note] Négatif sur la Note Kfet'
reply_to = ['tresorerie.bde@lists.crans.org',]

if __name__ == '__main__':
    #  Chargement du template de mail
    env = Environment(loader=PackageLoader('mail', 'templates'))
    template = env.get_template('template_recouvrement')

    # Préparation de la connexion à la base de données
    con, cur = BaseFonctions.getcursor()
    cur.execute("""
SELECT comptes.idbde, comptes.nom, comptes.prenom, comptes.solde, comptes.mail
FROM (
        (
            SELECT DISTINCT comptes.idbde
            FROM comptes
            JOIN adhesions ON adhesions.idbde = comptes.idbde
            WHERE comptes.solde < -500
            AND adhesions.annee IN (2014, 2015)
            AND comptes.type = 'personne'
        )
        EXCEPT
        (
            SELECT DISTINCT comptes.idbde
            FROM comptes
            JOIN adhesions ON adhesions.idbde = comptes.idbde
            WHERE adhesions.annee = 2016
        )
    ) AS resultats
JOIN comptes ON comptes.idbde = resultats.idbde
ORDER BY idbde
""")

    for user in cur.fetchall():
        # Récupération des inscriptions dans la base
        body = template.render(user=user)
        queue_mail(emetteur, [user["mail"],], objet, body, cc=[], replyto=reply_to)
