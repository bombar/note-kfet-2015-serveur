#!/usr/bin/python
#-*- coding: utf-8 -*-
from __future__ import unicode_literals
import sys
from jinja2 import Environment, PackageLoader
import psycopg2, psycopg2.extras

if '/home/note/note-kfet-2015-serveur/mail/' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/mail/')
from mail import queue_mail
sys.path.append('../serveur')
import BaseFonctions

con, cur = BaseFonctions.getcursor()
emetteur = 'notekfet2015@crans.org'
objet = 'Passation et droits note'
reply_to = 'respo-info.bde@lists.crans.org'

if __name__ == '__main__':
    env = Environment(loader=PackageLoader('mail', 'templates'))
    template = env.get_template('template_fin_droits')
    cur.execute("SELECT mail, pseudo FROM comptes WHERE (droits NOT IN ('basic', '') OR supreme OR surdroits != '') AND type != 'club' AND idbde > 0 AND idbde != 3508 AND NOT deleted;")
    liste_bh = cur.fetchall()

    for bh in liste_bh:
        contexte = {
                'name' : bh['pseudo'],
                   }

        body = template.render(**contexte)
        queue_mail(emetteur, [bh['mail'],], objet, body, cc=[], replyto=[reply_to,])
