#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""Script utile pour générer un premier user dans la base de données"""

import sys
if '/home/note/note-kfet-2015-serveur/serveur' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/serveur')
import BaseFonctions
import socket
import getpass
import re

def create_user(pseudo, passwd, nom, prenom, mail):
    """Crée un nouvel user dans la base de données"""

    # On ouvre une nouvelle connexion à la base de données
    con, cur = BaseFonctions.getcursor()

    # On utilise RETURNING idbde pour récupérer l'idbde que la base a généré
    cur.execute("""INSERT INTO comptes 
    (pseudo, passwd, nom, prenom, mail, droits, surdroits, supreme)
    VALUES (%s, %s, %s, %s, %s, 'all', 'all', True) RETURNING idbde;""" 
        ,  (pseudo, passwd, nom, prenom, mail))

    idbde = [cur.fetchone()["idbde"]]
    try:
        s = socket.socket()
        s.connect(('note.crans.org', 80))
        ip = s.getsockname()[0]
    except:
        ip = "127.0.0.1"
    params = {}
    params["pseudo"] = pseudo
    params["nom"] = nom
    params["prenom"] = prenom
    params["mail"] = mail
    BaseFonctions.log(ip, "(not logged)", "create_manual_user", cur, params, idbde)
    cur.execute("COMMIT;")


if __name__ == '__main__':
    while True:
        pseudo = raw_input("Pseudo: ")
        if BaseFonctions.pseudo_libre(pseudo):
            break
        else:
            print(u"Ce pseudo est déjà pris...")
    while True:
        password = getpass.getpass("Mot de passe: ")
        if getpass.getpass("Confirmation du mot de passe: ") == password:
            break
        else:
            print(u"Les deux mots de passe sont différents, veuillez réessayer...")
    while True:
        nom = raw_input("Nom: ")
        prenom = raw_input("Prenom: ")
        if (nom == '') or (prenom == ""):
            print("Un nom ou un prénom ne peut pas être vide")
        else:
            break
    while True:
        mail = raw_input("email: ")
        if not (re.match(ur'[^@]+@.+\..+', mail)):
            print("Mail invalide...")
        else:
            break
    password = BaseFonctions.hash_pass(password)
    create_user(pseudo, password, nom, prenom, mail)
    

