#!/bin/bash

# Pour exporter le schéma de la base de données et obtenir une base
# note kfet vide et utilisable.
# C'est mieux qu'un script la recréant intégralement, qu'on ne maintient pas.
# Contient des données minimalistes pour que la BDD soit utilisable.

# Attention, crache sur la sortie standard.

pg_dump -s note

TABLENAME=comptes_toexport_$RANDOM

psql note -c "CREATE TABLE $TABLENAME AS
    SELECT *
    FROM comptes
    WHERE idbde <= 0
;" > /dev/null # Ça affiche 'SELECT 5', mais on s'en tape

psql note -c "UPDATE $TABLENAME SET solde=0;" > /dev/null

pg_dump note --table=$TABLENAME --data-only | sed "s/$TABLENAME/comptes/"

pg_dump note --table=configurations --data-only

psql note -c "DROP TABLE $TABLENAME;" > /dev/null
