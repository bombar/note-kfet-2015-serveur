#!/bin/bash
echo "Nombre de 1A et non-1A aux WEIs (dont 4A+)"
psql note -c "SELECT adhesions.annee, SUM(CASE adhesions.annee=pa.annee WHEN true THEN 1 ELSE 0 END) AS en1a, SUM(CASE adhesions.annee=pa.annee WHEN false THEN 1 ELSE 0 END) AS pas1A, SUM(CASE adhesions.annee-pa.annee>=4 WHEN true THEN 1 ELSE 0 END) AS vieux FROM adhesions JOIN premiere_adhesion AS pa ON adhesions.idbde=pa.idbde WHERE wei GROUP BY adhesions.annee ORDER BY adhesions.annee;"
