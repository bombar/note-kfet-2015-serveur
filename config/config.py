#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Variables de configurations propres au fonctionnement du serveur NoteKfet2015.

Certaines variables de configuration sont cependant dans la table ``configurations`` de la base de données.

"""

import os

### Base de données
#: Nom de la base PostgreSQL
database = "note"
#: Nom de l'utilisateur PostgreSQL
pgsql_user = "note"
#: Nom de la base PostgreSQL où sont enqueués les mails
database_mails = "nkmails"

### Paramètres Web
#: L'adresse à laquelle on est censé trouver l'interface django de la note.
url_note = "https://note.crans.org/note/"
#: L'adresse à laquelle on peut réinitialiser son mot de passe sur l'interface django
url_regen_password = url_note + "regen_pw/"
#: L'adresse à laquelle on confirm les adresses mail
url_confirm_email = url_note + "confirm_email/"

### Paramètres de debug
#: Quand on est en mode debug, certaines choses n'ont pas le même comportement.
debug = True
#: Remplace l'adresse de destination des mails quand on est en mode debug.
debug_sendmail_to = [u"bombar@crans.org"]

# NB : en debug, ne pas envoyer de caractères non ascii, en général ça marche en local, mais pas plus...
#: Est-ce qu'il faut débuguer sur stdout ?
debug_stdout = True
#: Est-ce qu'il faut débuguer dans le logfile ? (cf plus bas pour son path)
debug_logfile = True
#: Niveaux de verbosité du débug (dans le fichier et en stdout (ignoré si le debug_stdout est à False))
#: Chaque niveau rajoute des choses qui sont loguées
#: 
#:  * ``0`` : rien (difficile de débuguer).
#:  * ``1`` : seulement quand quelque chose a été modifié (coïncide avec les appels à log).
#:  * ``2`` : les erreurs "ennuyeuses", c'est-à-dire, qui ne devrait jamais survenir dans une note sans bug…
#:  * ``3`` : toutes les erreurs/commandes échouées aussi sont loguées.
#:  * ``4`` : les commandes réussies (mais qui ne font pas de modification) également.
#:  * ``5`` : tout (les réponses aux :py:meth:`Server.mayi` et les messages de thread lancé, communication entre Server, attente du départ des clients.)
#: 
#: (les messages de démarrage/arrêt apparaissent dans tous les niveaux > 0)
debug_level_logfile = 5
#: debuglevel pour stdout
debug_level_stdout = 5

### En cas de lancement en mode daemon
#: Fichier où rediriger stdout quand le serveur est lancé en mode daemon.
default_stdout = "/var/log/note/full_log.log"
#: Fichier où enregistrer le pid du processus quand le serveur est lancé en mode daemon.
pidfile = "/var/run/note/serveur.pid"

### Emplacement des fichiers
#: Répertoire de base du serveur NoteKfet2015
basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#: Répertoire contenant le code du serveur
serverdir = os.path.join(basedir, "serveur/")
#: Répertoire où stocker les photos
photosdir = os.path.join(basedir, "photos/")

#: Path du fichier ``authdb`` (pour authentifier les utilisateurs spéciaux)
authfile = os.path.join(serverdir, "authdb.json")
#: Path du fichier de log
logfile = os.path.join(serverdir, "server.log")
#: Path du fichier contenant les erreurs des checks d'intégrité
integrityerrorfile = os.path.join(basedir, "integrite/errorfile")
#: Path du fichier contenant les logs des checks d'intégrité
integritylogfile = os.path.join(basedir, "integrite/logfile")

#: Path de la clé SSL
server_keyfile = os.path.join(basedir, "keys/note-kfet-2015-serveur.key")
#: Path du certificat SSL
server_certfile = os.path.join(basedir, "keys/note-kfet-2015-serveur.crt")


### Versioning
#: Liste des clients que le serveur accepte (ie = compatible).
#: Évidemment on peut toujours se connecter à la main, à nos risques et périls.
clients_whitelist = [u'manual', u'Python Client alpha', u'test_client', u'HTTP Django', u'Basile', u"digicode"]

### Paramètres d'écoute
#: Adresses IP sur lesquelles écouter
listen_bind = ["0.0.0.0"]
#: Port sur lequel écouter
listen_port = 4242

#: Taille maximale d'une requête
request_max_size = 2000000

### Trusting
#: IPs auxquelles on fait confiance pour cafter l'IP de leur utilisateur
trusted_ips = ["127.0.0.1"]

#: Comptes qui ne peuvent se connecter que depuis certaines IPs
limited_accounts = {
        3508 : ["10.54.4.194", "2a0c:700:0:21:329c:23ff:fe23:cd42",        # compte "note" seulement depuis kfet.crans.org
            "10.54.4.189", "2a0c:700:0:21:52e5:49ff:fea6:70b2",],      # ou video-kfet.crans.org
    }

### Mots de passe
#: Longueur d'un token envoyé par mail pour régénérer un mot de passe
token_regenerate_password_size = 20

### Les droits et leurs alias
#: Il y a plein de droits, mais certains méta-droits sont en fait des alias pour plein d'autres droits
#: on ne parle ici que des droits pour les utilisateurs bdd.
#: 
#: Les alias ``"all"`` et ``"root"``  ne sont pas définis ici, mais hardcodés. Ils valent pour tous les droits.
#: (attention, ça veut dire **vraiment tout** , y compris ``"chgpass"``, mais il est bien cloisonné
#: droits/surdroits et ne bypass pas supreme)
#: 
#: ``"myself"`` est un droit sépcial qui signifie "Je veux avoir l'accès à mon propre compte".
#: Quand on se connecte "pour laisser" on ne le met pas dans son masque.
droits_aliases_bdd = {
"basic"     : ["login", "myself", "preinscriptions", "dons", "activites", "invites", "remboursement", "wei", "liste_droits"],
"note"      : ["get_boutons", "inscriptions", "consos", "get_photo", "transferts", "credits", "retraits", "quick_search", "historique_transactions", "transactions"],
"comptes"   : ["search", "adherents_weak", "aliases", "historique_pseudo", "update_photo"],
"boutons"   : ["create_bouton", "update_bouton", "delete_bouton"],
"admin"     : ["activites_admin", "invites_admin", "adhesions_admin", "adherents_strong", "wei_admin", "full_search", "supprimer_compte", "remboursement_admin"],
#: Ces droits ne rentrent pas dans un alias, mais on a besoin de les connaître quand même.
#: Un alias commençant par un _ est un pseudo-alias. Ils ne peuvent pas être utilisés comme alias.
#: C'est-à-dire que donner le droit "_noalias" à quelqu'un ne lui donnera pas les droits ``"wei"``, par exemple.
"_noalias"  : ["forced", "overforced", "transactions_admin", "chgpass", "digicode", "tresorerie"]
}
#: Liste des alias. Permet de forcer leur ordre d'affichage.
droits_aliases_bdd_keys = ["basic", "note", "comptes", "boutons", "admin"]
#: Les alias de droits sont différents pour les utilisateurs spéciaux.
droits_aliases_special = {
"basic"     : droits_aliases_bdd["basic"],
"note"      : droits_aliases_bdd["note"] + droits_aliases_bdd["comptes"]
}
#: Droits accessibles uniquement aux utilisateurs spéciaux
special_privileges = ["die", "who", "adduser", "deluser", "users", "surdroits", "speak", "broadcast"]
#: On ne peut pas supprimer un compte qui a un de ces droits si on ne les as pas soi-même.
no_delete_droits = ["chgpass", "supprimer_compte"]

#: Données sur le compte qu'on peut quand même demander dans un whoami si on n'a pas le droit myself
minimum_account_data = ["idbde", "pseudo"]

### Timeouts
#: Temps d'inactivité (en secondes) au bout duquel un utilisateur perd la possibilité d'utiliser un droit.
#: Le droit "alive" signifiant "être authentifié"
inactivity_timeout = {"alive" : 30*60}

### Mails
#: Contenu du champ From des mails envoyés
mails_from = "note-galois@crans.org"
#: Mailing-list des respo-info
ml_respo_info = "bombar@crans.org"
#: Adresses mails recevant les rapports en cas de problème
mails_problem_to = [ml_respo_info]
#: Destinataires des mails en cas de problème de cohérence de la base.
mails_integrity_problem = [ml_respo_info]
#: Destinataires des mails en cas de comptes multiples à la réinitialisation d'un mot de passe
mails_generate_password_duplicate = [ml_respo_info]
#: Pour contacter les trésoriers du BDE
ml_tresoriers = "bombar@crans.org"
#: Pour contacter les responsables des activités du BDE
ml_respo_pot = "bombar@crans.org"
#: Destinataires pour lesquels on mettra un "X-Ack: No" pour éviter les réponses
#: automatiques de certaines mailing-lists
to_noack = [ml_tresoriers]
#: Si un destinataire matche cette regexp, on ajoute un header "Precedence: bulk"
#: pour ne pas recevoir les moderation notices
precedence_bulk_regex = '[^ ]+@lists.crans.org'

### Transactions
#: Types de transactions possibles
types_transactions = [u"transfert", u"bouton", u"don", u"crédit", u"retrait", u"adhésion", u"remboursement"]
#: Le nombre de transactions tansmises quand on demande un :py:meth:`ServeurFonctions.historique_transactions` avec paramètre ``"last"``
buffer_transactions_size = 50

### Wiki
#: Pseudo wiki *(Le mot de passe est importé du module secrets)*
wiki_pseudo = 'NoteKfet2015'
from secrets import wiki_password
#: Page wiki générant le calendrier des activités
wiki_calendar_raw = "VieBde/PlanningSoirees/LeCalendrier"
#: Page wiki recensant le planning des activités
wiki_calendar_humanreadable = "VieBde/PlanningSoirees"

### Photos
#: Résolution limite des photos
photo_max_size = 1000*1024
#: Seul ces formats de photo sont pris en compte, les autres seront rejetés.
photo_allowed_formats = ["png", "jpg", "jpeg", "gif", "tiff", "eps", "tga", "ico", "bmp", "xcf"]

### Liste d'invités
#: Template html pour la liste des invités
html_table_template = u"""<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>%(titre)s</title>
<style type="text/css">
table
{
    border-collapse: collapse;
    border: 1px solid black;
    vertical-align: center;
    margin: auto;
}
th
{
    border: 1px solid black;
    text-align: center;
    padding: 5px 5px 5px 5px;
}
td
{
    border: 1px solid black;
    padding: 5px 5px 5px 5px;
}
h1
{
    text-align: center;
    margin: 50px;
}
</style>
</head>
<body>
<p>Liste générée le %(date)s à %(heure)s</p>
<h1>%(titre)s</h1>
<table style="border: 1px solid black; margin: auto">
%(table_header)s
%(table_content)s
</table>
</body>
</html>"""

#: html pour le header de liste d'invités
html_invites_th_template = u"""<tr><th>Nom</th> <th>Prénom</th> <th>Responsable</th> <th>Nombre d'invitations</th></tr>\n"""

#: html pour une ligne de liste d'invités
html_invites_tr_template = u"""<tr><td>%(nom)s</td> <td>%(prenom)s</td> <td>%(responsable)s</td> <td>%(times)s</td></tr>\n"""

#: Path du binaire wkhtmltopdf
binary_wkhtmltopdf = u"/usr/bin/wkhtmltopdf"

### Secrets
from secrets import secret_key
