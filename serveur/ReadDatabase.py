#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Définition de quelques méthodes de recherche/accès à la base de données."""

import psycopg2
import psycopg2.extras
import datetime

# module qui définit les erreurs
import ExceptionsNote
# module qui définit des fonctions essentielles
import BaseFonctions

def _get_compte(quelparam, valeurparam):
    """Pour usage interne. Récupère le compte par idbde ou par pseudo"""
    req = "SELECT * FROM comptes WHERE %s = %%s;" % (quelparam)
    con, curseur = BaseFonctions.getcursor()
    curseur.execute(req, (valeurparam,))
    result = curseur.fetchone()
    if (result == None):
        raise ExceptionsNote.Error404((u"%s Unknown : %s" % (quelparam, valeurparam)).encode("utf-8"))
    else:
        for (k, v) in result.items():
            if (type(v) == str):
                result[k] = v.decode("utf-8")
        return result

def get_compte(idbde):
    """Récupère les infos du compte n° ``idbde`` dans la base de données et le renvoie sous forme d'un dictionnaire.
       
       Prend soin de transformer les champs texte en unicode.
       
       """
    try:
        idbde = int(idbde)
    except:
        raise ExceptionsNote.TuTeFousDeMaGueule(
             "l'idbde fourni (%s) n'est pas convertible en entier..." % idbde)
    return _get_compte("idbde", idbde)

def get_display_info(idbde):
    """       Retourne les infos d'un compte en rajoutant le tag negatif donnant des infos sur la négativité du compte :
        * 0 : en positif
        * 1 : solde_negatif > solde > solde_tres_negatif
        * 2 : solde_tres_negatif > solde > solde_pas_plus_negatif (forced sera nécessaire)
        * 3 : solde_pas_plus_negatif > solde (overforced sera nécessaire)"""
    compte = dict(get_compte(idbde))
    con, cur = BaseFonctions.getcursor()
    cur.execute("SELECT solde_negatif, solde_tres_negatif, solde_pas_plus_negatif FROM configurations WHERE used = true;")
    neg, tres_neg, pas_plus_neg = cur.fetchone()
    solde = compte["solde"]
    if solde >= neg:
        compte["negatif"] = 0
    elif solde >= tres_neg:
        compte["negatif"] = 1
    elif solde >= pas_plus_neg:
        compte["negatif"] = 2
    else:
        compte["negatif"] = 3
    return compte

def get_compte_by_pseudo(pseudo):
    """Récupère un compte par son pseudo."""
    return _get_compte("pseudo", pseudo)

def like_replace(chain):
    """Échappe les % et _ avec = (et donc = avec ==) pour pouvoir faire un LIKE."""
    # _ et % étant des caractères spéciaux pour LIKE, on a besoin de les échapper,
    # on choisit par convention de les échapper avec =, qu'on doit donc lui-même échapper (fais au moins semblant de suivre ce que je dis)
    # La syntaxe est "LIKE 'a==10=%%' ESCAPE '='"
    return chain.replace('=', '==').replace('%', '=%').replace('_', '=_')

def search(dico, give_alias, search_alias, give_historique, search_historique, old, insensitive, exact=0, orderby="nom", conj=False):
    """Effectue une recherche avancée sur la tables des comptes.
       
       ``dico = { <champ1> = <valeur1>, …}``
       
       Recherche les comptes dont au moins un des champs spécifiés "correspond" la valeur demandée.
       "correspond" signifiant :
       
        * correspondance sur le début si ``exact = 1``, match exact si ``exact = 2``, n'importe où si ``exact = 0`` (défaut)
        * insensible à la casse si ``insensitive``
       
       Renvoie une liste (ordonnée selon le champ ``orderby``) contenant un dictionnaire pour chaque compte trouvé.
       
       On peut utiliser les switchs suivants :
        * ``give_alias`` : renvoie également les alias des comptes.
        * ``search_alias`` : cherche à matcher également parmi les alias.
        * ``give_historique`` : renvoie également les anciens pseudos des comptes.
        * ``search_historique`` : cherche à matcher également parmi les anciens pseudos.
        * ``old`` : cherche aussi dans les comptes qui ne sont pas à jour d'adhésion.
        * ``conj`` : recherche conjonctive.
       
       """
    # NB : pas besoin de checker le champ orderby, il n'est jamais fourni par le client
    faux_champs = ["alias", "historique", "section"]
    # like-échappement
    dico = {k: like_replace(unicode(v)) for (k, v) in dico.items()}
    # Ensuite on crée le filtre like, qui dépend du flag insensitive
    likefilter = "I" * insensitive + "LIKE"
    if (exact == 0):
        # 0 = default
        exactfilter = "%%%s%%"
    elif (exact == 1):
        # 1 = begin
        exactfilter = "%s%%"
    elif (exact == 2):
        # 2 = exact
        exactfilter = "%s"
    #On gere la conjonction
    whereconnector = " OR "
    if conj:
        whereconnector = " AND "
    # on passe l'exactfilter dans les valeurs de recherche, comme ça il seront correctement refilés aux %(field)s
    dico = {k: exactfilter % (v) for (k, v) in dico.items()}
    # On caste tous les champs sur du text et comme ça on peut faire les LIKE correctement
    # évidemment il faut penser à exclure les "faux champs" (alias et historique)
    # et la section est gérée un peu différemment
    whereclauselist = []
    if "section" in dico.keys():
        whereclauselist = ["section(idbde) %s %%(section)s ESCAPE '='" % (likefilter)]
    whereclause = whereconnector.join(whereclauselist + ["CAST(%s AS text) %s %%(%s)s ESCAPE '='" % (field, likefilter, field) for field in dico.keys() if field not in faux_champs])
    if not whereclause:
	    #Si aucune clause n'a été cochée, on ne renvoie rien (na!)
    	whereclause = "false"
    # On a alors une whereclause avec des tas de %(field)s dedans
    # On gère les flags a et h
    if give_alias:
        # il faudra changer la partie SELECT
        give_aliases = ", (SELECT string_agg(alias, ', ' order by alias) FROM aliases WHERE aliases.idbde = comptes.idbde) AS aliases"
    else:
        give_aliases = ""
    if give_historique:
        # il faudra changer la partie SELECT
        # C'est-à-dire qu'on prend les pseudos "avant" si ils sont encore "valide"
        give_historiques = ", (SELECT string_agg(avant, ', ' order by avant) FROM historique WHERE historique.idbde = comptes.idbde AND historique.valide) AS historiques"
    else:
        give_historiques = ""
    # On gère les flags A et H
    subreqs = []
    if search_alias:
        # On prépare dans la sous-requête les idbde des alias qui matchent
        subreqs.append("SELECT idbde FROM aliases WHERE alias %s %%(alias)s ESCAPE '='" % (likefilter,))
    if search_historique:
        # On prépare dans la sous-requête les idbde des historiques encore valables qui matchent
        subreqs.append("SELECT idbde FROM historique WHERE avant %s %%(historique)s ESCAPE '=' AND historique.valide" % (likefilter,))
    subreqs.append("SELECT idbde FROM comptes WHERE %s" % (whereclause,))
    # On gère le flag o
    if old:
        oldclause = ""
    else:
        # On n'a pas précisé qu'on voulait aussi les old_accounts = comptes
        # pas à jour d'adhésion donc on rajoute le test isAdherent
        # NB: il est prévu qu'un club n'expire pas
        # Attention, comme isAdherent renvoie un record à 3 valeurs, il faut
        # récupérer celle qui nous intéresse
        oldclause = " AND (SELECT answer FROM isAdherent(comptes.idbde))"
    if "bloque" in dico.keys():
        # Si on recherche sur le champ bloque, il n'y a rien à ajouter…
        bloqueclause = ""
    else:
        # Mais par défaut, on ne renvoie que les comptes non-supprimés
        bloqueclause = " AND NOT comptes.deleted"
    orderclause = "ORDER BY %s" % (orderby)
    # sous-requête formé d'une union de plusieurs recherches
    subreq = " UNION ".join(["(%s)" % r for r in subreqs])
    req = """
        SELECT idbde, nom, prenom, pseudo, solde, mail, section(idbde) AS section%s%s
        FROM comptes
        WHERE idbde >= 0
        AND idbde IN (%s)
        %s
        %s
        %s;
        """ % (give_aliases, give_historiques, subreq, oldclause, bloqueclause, orderclause)
    # On peut filer le dico à la whereclause qui contient bien des %(field)s, c'est bon
    con, cur = BaseFonctions.getcursor()
    cur.execute(req, dico)
    l = cur.fetchall()
    return l

def _is_pg_regexp(pattern):
    """Renvoie ``True`` si ``pattern`` est une regexp correcte au sens de PostGreSQL."""
    assert(isinstance(pattern, basestring))
    con, cur = BaseFonctions.getcursor()
    try:
        cur.execute("SELECT '' ~ %s;", (pattern,))
    except psycopg2.DataError:
        return False
    else:
        return True

def _forge_test(test_field, include_regex=False):
    """Construit le test WHERE faisant un matching ILIKE entre ``test_field`` et le terme de recherche.
       Il faudra utiliser la clé ``"term_liked"`` pour le term LIKE-échapé avec un ``'"'``.
       
       Si ``include_regex`` est à ``True``, le test fera un OR avec un matching regex case-insensitive
       de la clé ``"term_regex"``.
       
       Il faut penser à like-échapper correctement ``term_liked`` et ajouter ^ à ``term_regex``
       avant l'exécution de la requête.
       """
    test = "%(test_field)s ILIKE %%(term_liked)s ESCAPE '='"
    if include_regex:
        test = "(" + test + " OR  %(test_field)s ~* %%(term_regex)s)"
    return test % {"test_field" : test_field}

def _forge_request(skeleton, pieces):
    """Construit la requête de recherche en plaçant dans le squelette ``skeleton``
       les morceaux du dico ``pieces``."""
    # On ajoute les séparateurs nécessaires si les morceaux spécifiques ne sont pas vides
    for k in pieces.keys():
        if k.startswith("specific"):
            if pieces[k]:
                separator = " AND " if k.endswith("where") else ", "
                pieces[k] = separator + pieces[k]
    return skeleton % pieces

def _degre_negatif(comptes):
    con, cur = BaseFonctions.getcursor()
    cur.execute("SELECT solde_negatif, solde_tres_negatif, solde_pas_plus_negatif FROM configurations WHERE used = true;")
    neg, tres_neg, pas_plus_neg = cur.fetchone()
    for i in range(len(comptes)):
        c = comptes[i]
        c = dict(c)
        solde = c["solde"]
        if solde >= neg:
            c["negatif"] = 0
        elif solde >= tres_neg:
            c["negatif"] = 1
        elif solde >= pas_plus_neg:
            c["negatif"] = 2
        else:
            c["negatif"] = 3
        comptes[i] = c



def quick_search(term, old=False, byidbde=False, hide_solde=False, exclude=None, byname=False):
    """Effectue une recherche simple :
       
        * sur les pseudos, les alias et l'historique
        * avec un filtre begin
        * case insensitive
        * on a juste le choix de préciser ``old`` ou pas (par défaut, on ne va pas chercher les comptes non à jour)
        * cas particulier : si ``byidbde`` est à True, on ne fait rien de tout ça mais on cherche sur les idbde
       
       Ne renvoie que ce qui a matché et l'idbde correspondant et l'appelle ``"terme"`` (que ce soit un pseudo, alias ou historique).
       Ne renvoie pas les comptes bloqués ou supprimés.
       
       Renvoie également le nom/prénom du compte en question.
       Ainsi que le solde, sauf si ``hide_solde`` est à ``True``.
       
       Le paramètre ``exclude`` est une liste d'idbde à exclure d'office.
       Ce paramètre vaut None si aucun résultat ne doit être exclus.
       
       Rajoute également un champ ``"was"`` qui peut être ``"pseudo"``, ``"alias"`` ou ``"historique"``.
       pour qu'on puisse savoir ce qui a matché (pour une mise en forme différente, par exemple)
       
       Donne également des infos sur la négativité du compte, dans le champ ``"negatif"`` :
        * 0 : en positif
        * 1 : solde_negatif > solde > solde_tres_negatif
        * 2 : solde_tres_negatif > solde > solde_pas_plus_negatif (forced sera nécessaire)
        * 3 : solde_pas_plus_negatif > solde (overforced sera nécessaire)
       
       Les idbde < 0 sont ignorés.
       
       """
    #: Morceaux à injecter dans le squelette de la requête
    pieces = {}
    #: Champs toujours SELECTionnés (c = table comptes)
    pieces["always_select"] = "c.pseudo, c.type, c.solde, c.prenom, c.nom, c.last_negatif, adh.section"
    #: Tables toujours présentes dans le FROM
    pieces["always_from"] = "comptes AS c , adhesions AS adh"
    #: À tester dans tous les cas
    pieces["always_where"] = "NOT (c.bloque OR c.deleted) AND c.idbde >= 0 AND c.last_adhesion = adh.id"
    #: Sauf si on est dans le cas de recherche byidbde, on affiche aussi les comptes bloqués
    if byidbde:
        pieces["always_where"] = "NOT c.deleted AND c.idbde >= 0"
    # On gère le flag o ici, parce qu'il est commun aux deux méthodes de recherche
    if old or byidbde:
        pieces["oldclause"] = ""
    else:
        pieces["oldclause"] = " AND (SELECT answer FROM isAdherent(c.idbde))"
    # On gère ici le flag x
    if exclude is not None:
        excludepseudo = " AND NOT (" + " OR ".join(["c.idbde=%d" % idbde for idbde in exclude]) + ")"
        excludealias = " AND NOT (" + " OR ".join(["a.idbde=%d" % idbde for idbde in exclude]) + ")"
        excludehistorique = " AND NOT (" + " OR ".join(["h.idbde=%d" % idbde for idbde in exclude]) + ")"
    else:
        excludepseudo = ""
        excludealias = ""
        excludehistorique = ""

    #: Structure de la requête
    req_skeleton = """SELECT %(always_select)s%(specific_select)s
                      FROM %(always_from)s%(specific_from)s
                      WHERE %(always_where)s%(specific_where)s%(oldclause)s;
                      """

    # On ne tente de faire un matching de regex que si le term fourni est une regexp comprise par pgsql
    # On ne fait pas le test dans le cas d'idbde puisque le terme donné est un int
    is_regex = not byidbde and _is_pg_regexp(term)
    # Map cas_de_recherche -> clauses spécifiques
    request_cases = {
        "pseudo" :     {"specific_select" : "c.pseudo AS terme, c.idbde",
                        "specific_from" : "",
                        "specific_where" : _forge_test("c.pseudo", is_regex) + excludepseudo},
        "alias" :      {"specific_select" : "a.alias AS terme, a.idbde",
                        "specific_from" : "aliases AS a",
                        "specific_where" : "a.idbde = c.idbde AND " + _forge_test("a.alias", is_regex) + excludealias},
        "historique" : {"specific_select" : "h.avant AS terme, h.idbde",
                        "specific_from" : "historique AS h",
                        "specific_where" : "h.idbde = c.idbde AND h.valide AND " + _forge_test("h.avant", is_regex) + excludehistorique},
        "idbde" :      {"specific_select" : "c.idbde AS terme, c.idbde",
                        "specific_from" : "",
                        "specific_where" : "CAST(c.idbde AS text) LIKE %(term_liked)s"},
        "nom" :        {"specific_select" : "c.nom AS terme, c.idbde",
                        "specific_from" : "",
                        "specific_where" : _forge_test("c.nom", is_regex)},
        "prenom" :     {"specific_select" : "c.prenom AS terme, c.idbde",
                        "specific_from" : "",
                        "specific_where" : _forge_test("c.prenom", is_regex)},
    }

    # Ensuite on différencie le cas idbde de l'autre
    if byidbde:
        # On transformer l'entier id en chaîne et on ajoute le "%" (= filtre begin)
        term = str(term)
        term_liked =  term + "%"
        term_regex = "^$" # inutilisé dans ce cas
        # On utilise le type de recherche idbde
        search_types = ["idbde"]
        # On ajoute un # devant les idbde trouvés
        post_transform_term = lambda x : "#" + str(x)
    else:
        # On transforme le terme en passant par le like-échappement et en ajoutant "%" (= filtre begin)
        term_liked = like_replace(term) + "%"
        term_regex = "^" + term
        # On recherche sur les pseudos, puis les alias, puis les historiques
        search_types = ["pseudo", "alias", "historique"] + (["nom", "prenom"] if byname else [])
        # On ne modifie pas les chaînes trouvées
        post_transform_term = lambda x : x
    con, cur = BaseFonctions.getcursor()
    results = []
    for search_type in search_types:
        pieces.update(request_cases[search_type])
        req = _forge_request(req_skeleton, pieces)
        cur.execute(req, {"term_liked" : term_liked, "term_regex" : term_regex})
        localresults = cur.fetchall()
        # On transforme les records en dico et on rajoute le champ "was"
        for i in range(len(localresults)):
            d = dict(localresults[i])
            d["terme"] = post_transform_term(d["terme"])
            d["was"] = search_type
            # Si le compte est un club, on vide nom et prénom pour obliger
            # l'utilisateur à les spécifier manuellement lors d'un crédit/retrait
            if d["type"] == u"club":
                d["nom"] = u""
                d["prenom"] = u""
            localresults[i] = d
        results += localresults
    # On a alors récupéré une liste de dico {"terme": <pseudo ou alias ou historique>, "idbde": <idbde>, ...}
    # qu'on va ordonner par ordre alphabétique de "terme"
    results.sort(key=lambda x: x["terme"].lower())
    # Limitations du nombre de résultats (sinon sort de la page)
    results = results[:50]
    # On s'occupe maintenant de regarder si tous ces gens sont en négatif ou pas
    # ça sert à les afficher de couleurs différentes
    # NB : le solde est bien en centimes
    if not hide_solde:
        _degre_negatif(results)
    else:
        # Rappel : tout utilisateur qui peut faire un don a le droit de faire un quick_search, mais pas de voir les soldes
        # donc on enlève les soldes dans ce cas
        for d in results:
            del d["solde"]

    for i in range(len(results)):
        if results[i]['last_negatif'] != None:
            results[i]['time_negatif'] = (datetime.datetime.now() - results[i]['last_negatif']).days
        else:
            results[i]['time_negatif'] = 'n' #troll

    if old:
        for i in range(len(results)):
            req = """ SELECT min(annee) AS premiere_adhesion FROM adhesions WHERE idbde = %(idbde)s ;"""
            cur.execute(req, results[i])
            min_year = cur.fetchone()["premiere_adhesion"]
            if min_year == None:
                vieux = False
            else:
                #Dis, c'est quoi être vieux ? Dans une colonne ans la table config
                vieux = (datetime.datetime.now().year - min_year) >= 4
            results[i]["vieux"] = vieux

    return results

def search_invite(term, idact):
    """ Fonction de recherche des invités à une activité
    term : terme de recherche. Match sur nom, prénomde l'invité et sur le pseudo de l'inviteur
    idact : id de l'activité concernée

    Retourne la liste des invités à cette activités avec le degré de négativité de leur inviteur"""

    invites = []
    con, cur = BaseFonctions.getcursor()
    term = term.replace("'", "''")
    cur.execute(u"""SELECT 'invite' AS type, i.id AS idbde, i.nom, i.prenom,
                    c.idbde AS Ridbde, c.nom AS Rnom, c.prenom AS Rprenom, c.pseudo AS pseudo, c.solde AS solde
                    FROM invites AS i
                    JOIN comptes AS c
                    ON i.responsable=c.idbde
                    WHERE i.activite={0} AND (i.nom ILIKE '{1}%' OR i.prenom ILIKE '{1}%' OR pseudo ILIKE '{1}%' OR c.nom ILIKE '{1}%' OR c.prenom ILIKE '{1}%');""".format(idact, term))
    invites = cur.fetchall()
    _degre_negatif(invites)

    return invites
